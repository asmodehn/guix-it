# guix-it

Minimal set of guix packages to get a running system to learn and improve our software knowledge collaboratively.

Although we loosely follow the historical timeline, we aim to quickly reach an "advanced proficiency in software development", hoping to stimulate the user to learn by him/herself and spread his/her knowledge.


## Use it now !

Add the GuixIT channel on top of the latest guix (likely the default on your machine)

Your `~/.config/guix/channels.scm` should contain :

```
;; Add guix-it to default channels.
(cons (channel
        (name 'pyros)
        (url "https://gitlab.com/asmodehn/guix-it.git")
        (branch "master"))
      %default-channels)

```

## Goals

Here are our (still moving) targets:

TODO : find sexy names for each Level => they will be urls for documentation, package names for installation, etc.

 Level 1 - Terminal : Mostly about how to use a linux system from the terminal (Guix)
 - Systeme: POSIX
 - Language: C
 - Editor: vi
 - Shell: bash


 Level 2 - GUI: About using Guix itself 
 - Systeme: Guix
 - Language: Scheme (Guile)
 - Editor: Emacs (Terminal + Editor + Evil mode)
 - Shell: eshell ? something successful and scheme-like ?
 - Maybe decide on a clean functional Window manager ?


 Level 3 - WebUI: About using Guix to build other systems via Mirage/Functoria
 - Systeme: Mirage
 - Language: OCaml (+HOL -> proofs)
 - Editor: Jupyter (Terminal + Editor + ...)
 - Shell ?


The applications to use for each level, enabling worldwide collaborative learning:
Level1 - Terminal: basic usage but from the console
- Email client
- IRC client
- Web client
- SSH client
- SVN client
- git client

Level2 - GUI: advanced usage, as a client, but also setup as a server owner/service provider.
- Email client & server
- News client & server
- IRC/XMPP client & server
- Web client & server
- SSH client & server
- svn client & server
- git client & server


Level3 - WebUI: distributed usage & practices, distributed computing concepts, local-first.
- Local Website production (leveraging jupyter) for teaching.
- ScuttleBut
- Matrix Client & Server
- ...


